package Answer1;

public class Car {
	
	private String name;
	
	public Car(){
		this.name = "Honda";
	} //default constructor
	
	public Car(String name){
		this.name = name;
	} //overloding
	

	public String toString(){
		return this.name;	
	}
	
	

}
