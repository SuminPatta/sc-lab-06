package Answer2;

public class Car02 extends Vehicle02 {
	
	private String name;
	
	public Car02(){
		super();
	} 
	
	public Car02(String name){
		this.name = name;
	} //overloading method 

	
	public void motor(){
		
	}
	
	public void wheel() {

	}
	
	public String toString(){
		return this.name;	
	}

}
