package Answer2;

public class Motercycle extends Vehicle{
	private String name;
	
	public Motercycle(){
		super();
	}
	public Motercycle(String name){
		this.name = name;
	}
	

	@Override
	public String motor() {
		return "1-Motor";
	} //overriding abstract methods

	@Override
	public String wheel() {
		return "2-wheels";
	} //overriding abstract methods
	
	public String toString(){
		return this.name;	
	}
	
	
	

}
