package Answer2;

public class Car extends Vehicle {
	
	private String name;
	
	public Car(){
		super();
	}
	public Car(String name){
		this.name = name;
	} //overloading method 

	@Override
	public String motor() {
		return "4-Motor";
	} //overriding abstract methods
	
	

	@Override
	public String wheel() {
		return "4-whells";
	}//overriding abstract methods
	
	public String toString(){
		return this.name;	
	}

}
