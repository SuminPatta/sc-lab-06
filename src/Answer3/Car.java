package Answer3;

public class Car {
	// answer 3.1
	/*private String color; // var with same name
	public void paintCar(){
		String color; // local var , shadow
	}*/
	
	private double wheel;
	public void carWheel(){
		String wheel;
		
	}
	
	// answer 3.2
	// different access modifier but have same name.
	public static void main(String[] args){
		Car1 c1 = new Car1();
		String var1 = c1.chair;// access modifier is public.
		Car2 c2 = new Car2();
		String var2 = c2.getChair(); // access modifier is private.
	}

}
